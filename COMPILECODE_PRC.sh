 
 #this script compiles the code for estimation of isochrones
 
 HEADERPATH='../HEADERFILES_PROJECT_PRC_ESTIMATION/'
 CODEPATH='../CODE_PROJECT_PRC_ESTIMATION/'
 
 g++ $HEADERPATH/Main_prc_estim.h $CODEPATH/Noise.cpp $CODEPATH/RungeKutta.cpp $CODEPATH/System.cpp $CODEPATH/Main_prc_estim.cpp -o PRC_ML -std=c++11 -O
