// ### RK FUNCTION DECLARATION FOR DATA GENERATION    ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

void rk1(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin);

void rk2(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin);

void rk3(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin);

void rk4var(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, double dt, double &pertVar, double &epsin);

void rk4(void (*derives)(vector<double> y, vector<double> &dydx, const int N, double &t, double &pertVar, double &epsin), vector<double> &y, const double Nsim, double &t, const int N, const double dt, double &pertVar, double &epsin);
