// ### CONSTANT DEFINITIONS DATA GENERATION           ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

const int N(2);
const double Nsim(1);
const double Ntime(300000);
const double Tbegin(0.0);
const double Tend(0.0);
const double dt(0.002);
const double yinit(1.2);
const double eps(10.0); //given as argv
const double Omega(sqrt(5.0));//sqrt(3.0));//frequency of forcing
const double Omegaint(1.6);//frequency of internal oscillation SL oscilator
const double pert(0.1);//given as argv

//### PARAMETERS FOR NEURONAL MODELS ###//

// MORRIS LECAR //
//Paramters from "Bifurcations in Morris–Lecar neuron model"
//               "FORCED SYNCHRONIZATION IN MORRIS–LECAR NEURONS"
//TYPEI
const double t_re(20.0);
const double C(20.0);
const double I(50.0/C*t_re);//#
const double gL(2.0/C*t_re);//#
const double gK(8.0/C*t_re);//#
const double gCa(4.0/C*t_re);//#
const double V1(-1.2);//#
const double V2(18.0);//#
const double V3(12.0);//###### -> class 1
const double V4(17.4);//#
const double VL(-60.0);//#
const double VK(-80.0);//#
const double VCa(120.0);//#
const double Phi (1.0/15.0*t_re);//#
//*/
//TYPEII
/*const double t_re(20.0);
const double C(20.0);
const double I(55.0/C*t_re);
const double gL(2.0/C*t_re);//#
const double gK(8.0/C*t_re);//#
const double gCa(4.0/C*t_re);//#
const double V1(-1.2);//#
const double V2(18.0);//#
const double V3(2.0);//###### -> class 2
const double V4(17.4);//#
const double VL(-60.0);//#
const double VK(-80.0);//#
const double VCa(120.0);//#
const double Phi (1.0/15.0*t_re);//#
//*/

//TYPE II "Comparison of Coding Capabilities of Type I and Type II Neurons 2004"
/*const double I(0.135);//0.01
const double gL(0.5);
const double gK(2.0);
const double gCa(1.1);
const double V1(-0.01);
const double V2(0.15);
const double V3(0.0167);
const double V4(0.25);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.7);
const double Phi (0.2);
//*/
//TYPEI
/*const double I(0.0718);//use 0.09 for periodic solution
const double gL(0.5);
const double gK(2.0);
const double gCa(1.0);
const double V1(-0.01);
const double V2(0.15);
const double V3(0.1);
const double V4(0.145);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.0);
const double Phi (0.333);//*/
//my parameters 
//TYPE II
/*const double I(0.05);
const double gL(0.5);
const double gK(2.0);
const double gCa(1.1);
const double V1(-0.01);
const double V2(0.21);
const double V3(-0.04);
const double V4(0.25);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.7);
const double Phi (0.2);
//*/
//TYPEI
/*const double I(0.075);
const double gL(0.5);
const double gK(2.0);
const double gCa(1.33);
const double V1(-0.01);
const double V2(0.15);
const double V3(0.1);
const double V4(0.145);
const double VL(-0.5);
const double VK(-0.7);
const double VCa(1.0);
const double Phi(0.333);
//*/
