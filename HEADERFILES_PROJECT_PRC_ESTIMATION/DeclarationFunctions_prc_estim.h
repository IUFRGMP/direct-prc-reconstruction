// ### FUNCTION DECLARATION FOR DATA GENERATION       ### //
// ###                                                ### //
// ### AUTHOR: ERIK GENGEL, University of Potsdam     ### //
// ###                                                ### //

#include "baseInclude.h"

//### DECLARATIONS IN CHRONOLOGICAL ORDER OF THE CODE ###//

vector<double> noiseGauss(double mean, double sigma);
void Initializer(vector<double> &y, vector<double> &dydx, double &t, const double yinit, const int N);
void sys(vector<double> yy, vector<double> &dyydx, const int N, double &t, double &pertVar, double &epsin);
void Henontrick(vector<double> yy, vector<double> &dyydx, const int N, double &t, double &pertVar, double &epsin);
void FindZero(vector<double> &y,double &t, double &pertVar, double &epsin);

//biological functions for the Morris-Lecar neuron model
double M_inf(double x);
double W_inf(double x);
double Lambda(double x);
