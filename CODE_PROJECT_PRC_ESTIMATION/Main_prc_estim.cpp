//### IMPLEMENTATION OF DATA GENERATION            ###//
//###                                              ###//
//### AUTHOR: ERIK GENGEL, University of Potsdam   ###//
//###                                              ###//
 
 #include "../HEADERFILES_PROJECT_PRC_ESTIMATION/Main_prc_estim.h"
 
 int main(int argc, char* argv[])
  {
   
   //NOTE: also give argv[1] as experiment  number;
   double pertin = strtod(argv[2],NULL);  //impulse amplitude of the stimulus
   double epsin = strtod(argv[3],NULL);   //stability parameter (if needed in SL, VdP, etc systems)
   int nst_a = int(strtod(argv[4],NULL)); //discretisation of period 
   double Iin = strtod(argv[5],NULL);     //bifurcation parameter of a model. e.g. baseline current

   //cout << "START OF PRC CALCULATION" << "\n";
   
   //variables
   vector<double> y(N);
   vector<double> yy(N);
   vector<double> dydx(N);
   //for PRC determination
   
   double t;
    
   std::stringstream out0; out0 << "PRC_true" << argv[1] << ".dat";
   std::string name0 = out0.str(); ofstream fout(name0,ios::out);
   fout.setf(ios::scientific); fout.precision(15);
  
   //### determine the period time ###//
  
   Initializer(y, dydx, t, yinit, N);
   
   std::stringstream out1; out1 << "PRC_true_data" << argv[1] << ".dat";
   std::string name1 = out1.str(); ofstream fout1(name1,ios::out);
   fout1.setf(ios::scientific); fout1.precision(15);

   //cout << "transient \n";
   double pertVar(Iin);//otherwise set to 0.0!
   for(int k=0;k<300000;k++)      //  transient for unperturbed system
    {
     rk4(sys,y,Nsim,t,N,dt,pertVar,epsin);//unperturbed system
     fout1 << t << " " << y[0] << " " << y[1] << "\n";
    }   
    
   fout1.close();

   //cout << "start FindZero \n";
    
   double t1(0.0), T(0.0), step_a(0.0), phi(0.0);
   for(int k=0; k<10; k++)
    {
     FindZero(y,t,pertVar,epsin); //find first zero crossing
     t1=t;
     FindZero(y,t,pertVar,epsin); //find second zero crossing
     T=t-t1;                //calculate Period time on cycle
    }
   step_a=T/nst_a;        //calculate integration step for later phase 
   int relaxes(30);       //set number of relaxation cycles for collective PRC
   
   //### Output the period time to file header
   fout << "#natural period T= " << T << "\n";

   t=0.0;
   rk4(sys,y,Nsim,t,N,step_a,pertVar,epsin); //make next step on unperturbed cycle
   double sumUpT(0.0), tt(0.0); 
   for(int k=0; k<nst_a-1;k++)//iterate over points in one cycle
    {
     tt=t; 
     yy[0]=y[0];//+pertin;//*cos(2.0*pi*t/T); 
     yy[1]=y[1]+pertin;//*sin(2.0*pi*t/T); //pert. approx. rectangul. to flow
     sumUpT = 0.0;//collect perturbed periods
     t1=0.0;
     for(int m=0; m<relaxes; m++)//iterate relaxation after perturbation in copy system
      {
       FindZero(yy,tt,pertVar,epsin); //find first zero crossing
       sumUpT +=tt-t1;
       t1=tt;
      }
     cout << "#################### next step= " << nst_a-k << "\n";
     fout << t*2.0*pi/T << " " << 2.0*pi*(double(relaxes) - sumUpT/T)/pertin << " " << y[0] << " " << y[1] << " " << "\n"; 
     rk4(sys,y,Nsim,t,N,step_a,pertVar,epsin); //make next step on unperturbed cycle
    }
   fout.close();
   
   return 0;
  }
